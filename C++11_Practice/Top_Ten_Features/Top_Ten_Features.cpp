/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Top_Ten_Features.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/30/2013
PROJECT    : 
DESCRIPTION: Code samples highlighting the C++11 standard.
//           http://www.codeproject.com/Articles/570638/Ten-Cplusplus11-Features-Every-Cplusplus-Developer
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>

class foo{

private:

  int x;

public:

  foo(){x = 0;}
  foo( int x_ ){x = x_;}

};

class bar{

private:

  int x;

public:

  bar( int x_ ){x = x_;}

};



int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // auto keyword: compiler attempts to infer correct data type                //
  ///////////////////////////////////////////////////////////////////////////////

  auto i = 42;        // i is an int
  auto l = 42LL;      // l is an long long
  auto p = new foo(); // p is a foo*


  ///////////////////////////////////////////////////////////////////////////////
  // range-based for-loops                                                     //
  ///////////////////////////////////////////////////////////////////////////////

  std::map<std::string, std::vector<int>> map;
  std::vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  map["one"] = v;
  
  // const reference to each element in map
  for(const auto& kvp : map){
    std::cout << kvp.first << std::endl;
      
    for(auto v : kvp.second){
      std::cout << v << std::endl;
    }
  }
  
  int arr[] = {1,2,3,4,5};

  for(int& e : arr){
    e = e*e;
  }

  return 0;
}

//END OF FILE
