/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : SI_Units.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/18/2013
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>

template <int M, int L, int T>
class Quantity{

public:
  
  constexpr Quantity(double val=0.0) : value(val){}
  
  constexpr Quantity(const Quantity& x) : value(x.value){}
  
  constexpr Quantity& operator+=(const Quantity& rhs){
    value += rhs.value;
    return *this;
  }
  
  constexpr Quantity& operator-=(const Quantity& rhs){
    value -= rhs.value;
    return *this;
  }
  
  constexpr double Convert(const Quantity& rhs){
    return value/rhs.value;                    
  }
  
  constexpr double getValue() const{
    return value;
  }        
  
private:    
  double value;    
};

template <int M, int L, int T>
constexpr Quantity<M,L,T> operator+(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return Quantity<M,L,T>(lhs)+=rhs;
}

template <int M, int L, int T>
constexpr Quantity<M,L,T> operator-(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return Quantity<M,L,T>(lhs)-=rhs;
}

template <int M1, int L1, int T1, int M2, int L2, int T2>
constexpr Quantity<M1+M2,L1+L2,T1+T2> operator*(const Quantity<M1,L1,T1>& lhs, const Quantity<M2,L2,T2>& rhs){
  return Quantity<M1+M2,L1+L2,T1+T2>(lhs.getValue()*rhs.getValue());
}

template <int M, int L, int T>
constexpr Quantity<M,L,T> operator*(const double& lhs, const Quantity<M,L,T>& rhs){
  return Quantity<M,L,T>(lhs*rhs.getValue());
}

template <int M1, int L1, int T1, int M2, int L2, int T2>
constexpr Quantity<M1-M2,L1-L2,T1-T2> operator/(const Quantity<M1,L1,T1>& lhs, const Quantity<M2,L2,T2>& rhs ){
  return Quantity<M1-M2,L1-L2,T1-T2>(lhs.getValue()/rhs.getValue());
}
 
template <int M, int L, int T>
constexpr Quantity<-M, -L, -T> operator/(double x, const Quantity<M,L,T>& rhs){
  return Quantity<-M,-L,-T>(x/rhs.getValue());
}
 
template <int M, int L, int T>
constexpr Quantity<M, L, T> operator/(const Quantity<M,L,T>& rhs, double x){
  return Quantity<M,L,T>(rhs.getValue()/x);
}

template <int M, int L, int T>
constexpr bool operator==(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return (lhs.getValue()==rhs.getValue());
}
 
template <int M, int L, int T>
constexpr bool operator!=(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return (lhs.getValue()!=rhs.getValue());
}

template <int M, int L, int T>
constexpr bool operator<=(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return lhs.getValue()<=rhs.getValue();
}

template <int M, int L, int T>
constexpr bool operator>=(const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return lhs.getValue()>=rhs.getValue();
}

template <int M, int L, int T>
constexpr bool operator< (const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return lhs.getValue()<rhs.getValue();
}

template <int M, int L, int T>
constexpr bool operator> (const Quantity<M,L,T>& lhs, const Quantity<M,L,T>& rhs){
  return lhs.getValue()>rhs.getValue();
}

typedef Quantity<1,0,0> Mass;
typedef Quantity<0,1,0> Length;
typedef Quantity<0,2,0> Area;
typedef Quantity<0,3,0> Volume;
typedef Quantity<0,0,1> Time;
typedef Quantity<0,1,-1> Speed;
typedef Quantity<0,1,-2> Acceleration;
typedef Quantity<0,0,-1> Frequency;
typedef Quantity<1,1,-2> Force;
typedef Quantity<1,-1,-2> Pressure;



constexpr Length meter(1.0);
constexpr Length decimeter = meter/10;
constexpr Length centimeter = meter/100;
constexpr Length millimeter = meter/1000;
constexpr Length kilometer = 1000 * meter;
constexpr Length inch = 2.54 * centimeter;
constexpr Length foot = 12 * inch;
constexpr Length yard = 3 * foot;
constexpr Length mile = 5280 * foot;
constexpr Frequency Hz(1.0);
 
constexpr Area kilometer2 = kilometer*kilometer;
constexpr Area meter2 = meter*meter;
constexpr Area decimeter2 = decimeter*decimeter;
constexpr Area centimeter2 = centimeter*centimeter;
constexpr Area millimeter2 = millimeter * millimeter;
constexpr Area inch2 =inch*inch;
constexpr Area foot2 = foot*foot;
constexpr Area mile2 = mile*mile;
 
constexpr Volume kilometer3 = kilometer2*kilometer;
constexpr Volume meter3 = meter2*meter;
constexpr Volume decimeter3 = decimeter2*decimeter;
constexpr Volume litre = decimeter3;
constexpr Volume centimeter3 = centimeter2*centimeter;
constexpr Volume millimeter3 = millimeter2 * millimeter;
constexpr Volume inch3 =inch2*inch;
constexpr Volume foot3 = foot2*foot;
constexpr Volume mile3 = mile2*mile;
 
constexpr Time second(1.0);
constexpr Quantity<0,0,2> second2(1.0);
constexpr Time minute = 60 * second;
constexpr Time hour = 60 * minute;
constexpr Time day = 24 * hour;
 
constexpr Mass kg(1.0);
constexpr Mass gramme = 0.001 * kg;
constexpr Mass tonne = 1000 * kg;
constexpr Mass ounce = 0.028349523125 * kg;
constexpr Mass pound = 16 * ounce;
constexpr Mass stone = 14 * pound;

constexpr Mass operator"" _kg(unsigned long long x)  { return Mass(static_cast<double>(x)); }  

constexpr Length operator"" _mm(long double x) { return x*millimeter; }
constexpr Length operator"" _cm(long double x)  { return x*centimeter; }
constexpr Length operator"" _m(long double x)  { return x*meter; }

constexpr Length operator"" _mi(long double x)  { return x*mile; }
constexpr Length operator"" _km(long double x)  { return x*kilometer; }

constexpr Speed operator"" _mps(long double x)  { return Speed(x); }
constexpr Speed operator"" _mph(long double x) { return x*mile/hour; }
constexpr Speed operator"" _kph(long double x) { return x*kilometer/hour; } 

#define ConvertTo(_x, _y) (_x).Convert(1.0_##_y) 

int main(){

  Mass myMass = 80*kg;
  std::cout << "my mass: " << myMass.Convert(kg) << " kg" << std::endl;
  std::cout << "my mass: " << myMass.Convert(stone) << " stone" << std::endl;
  std::cout << "my mass: " << myMass.Convert(pound) << " pounds" << std::endl;

  std::cout << (20 * mile).Convert(kilometer) << std::endl;
  std::cout << ConvertTo(20.0_mi, km) << std::endl;

  return 0;
}

//END OF FILE
