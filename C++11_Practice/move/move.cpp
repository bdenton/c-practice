/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : move.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 02/11/2014
PROJECT    : 
DESCRIPTION: Demonstrate std::move()
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <vector>
#include <string>

int main(){
  
  std::string x = "x", y = "y", A = "A";
  const std::string z = "z";
  
  // Create a vector of integers
  std::vector< std::string > vec;
  vec.resize( 3 );


  std::cout << "Print value of x, y, and z before copying/moving to vector" << std::endl;
  std::cout << x << std::endl;
  std::cout << y << std::endl;
  std::cout << z << std::endl;
  std::cout << std::endl;


  // Push x and y into vector
  vec[0] =  x;
  vec[1] = std::move( y );
  vec[2] = std::move( z );

  std::cout << "Print values of vector elements" << std::endl; // note use of auto and range-based loop
  for( auto v : vec )
    std::cout << v << std::endl;
  std::cout << std::endl;


  std::cout << "Print value of x, y, and z after copying/moving to vector" << std::endl;
  std::cout << x << std::endl;// Unchanged because value was copied to vector
  std::cout << y << std::endl;// Now empty because value was moved to vector
  std::cout << z << std::endl;// Unchanged because const forces copy even if std::move() called
  std::cout << std::endl;

  std::cout << "Move is sometimes implemented as a swap" << std::endl;
  vec[0] = std::move( A );
  std::cout << "Print values of vector elements" << std::endl;
  for( auto v : vec )
    std::cout << v << std::endl;
  std::cout << std::endl;
  std::cout << "Print A" << std::endl;
  std::cout << A << std::endl;
  std::cout << std::endl;

  // std::move() can be useful because it can take the result of a temporary
  // evaluation (e.g. the creation of the return value for a function) and assign
  // this object to an lvalue directly rather than making a copy. In the above 
  // example the content of y was moved into the vector rather than first
  // creating a new temporary string variable and populating it with the same 
  // string value as y and then pushing this into the vector. Note that move 
  // semantics may be ignored by the compiler when standard copying procedures
  // could be just as efficient as is the case with certain primitive data types
  // link int.


  return 0;
}

//END OF FILE
