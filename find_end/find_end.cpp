#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int main(){

  std::vector< std::string>::iterator it1, it2;

  std::vector< std::string > letters;
  letters.push_back( "A" );
  letters.push_back( "A" );
  letters.push_back( "A" );
  letters.push_back( "A" );
  letters.push_back( "B" );
  letters.push_back( "B" );
  letters.push_back( "B" );
  letters.push_back( "C" );
  
  it1 = std::find( letters.begin(), letters.end(), "D" );

  if( it1 == letters.end() )
    std::cout << "Not found" << std::endl;

  else{
    it2 = std::find_end( letters.begin(), letters.end(), it1, it1 + 1 );
    
    size_t first = std::distance( letters.begin(), it1 );
    size_t last = std::distance( letters.begin(), it2 );
  
    std::cout << "first: " << first << std::endl
	      << "last:  " << last << std::endl;
  }

  return 0;

}
