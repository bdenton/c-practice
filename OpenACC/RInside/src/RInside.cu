/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : RInside.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 05/03/2013
PROJECT    : CUDA practice
DESCRIPTION: Add two vectors.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <RInside.h>

//template<typename T>
void print( thrust::host_vector< int >& v ){

  thrust::host_vector< int >::iterator it;  

  for( it = v.begin(); it != v.end(); ++it )
    std::cout << *it << " ";
}

__global__ void cuda_mean( const int* v1,
		   const int* v2,
		   double* v3,
		   int size ){

  //int global_thread_id = blockIdx.x * blockDim.x + threadIdx.x;

  // if( global_thread_id < size )
  //   v3[global_thread_id] = v1[global_thread_id] + v2[global_thread_id];
}



int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  unsigned int N = 10;
  
  thrust::host_vector< int > host_A;
  thrust::host_vector< int > host_B;
  thrust::host_vector< double > host_C;
  
  // host_A.resize( N );
  // host_B.resize( N );
  // host_C.resize( N );
  
  // for( int i = 0; i < N; ++i )
  //   host_A[ i ] = i;

  
  // for( int j = 0; j < N; ++j )
  //   host_B[j] = 100 + j;
  

  // // Print array contents before GPU computation
  // print( host_A );
  // std::cout << std::endl;

  // print( host_B );
  // std::cout << std::endl;

  // //print( host_C );
  // std::cout << std::endl;

  // ///////////////////////////////////////////////////////////////////////////////
  // // Device data                                                               //
  // ///////////////////////////////////////////////////////////////////////////////

  // // Allocate device memory and copy data from host
  // thrust::device_vector< int > device_A = host_A;
  // thrust::device_vector< int > device_B = host_B;
  // thrust::device_vector< double > device_C = host_C;

  
  // // Thrust is a host-side abstraction only. You can create Thrust vectors and
  // // other data types in memory, but you must pass C-style pointers to the 
  // // CUDA kernel function. Thrust provides the function raw_pointer_cast() for the
  // // purpose of creating pointers to Thrust data objects.
  
  // // Create C-style pointers to Thrust device vectors
  // int* device_A_ptr = thrust::raw_pointer_cast( &device_A[0] );
  // int* device_B_ptr = thrust::raw_pointer_cast( &device_B[0] );
  // double* device_C_ptr = thrust::raw_pointer_cast( &device_C[0] );


  // // Call CUDA kernel
  // cuda_mean<<<1, 32>>>( device_A_ptr, device_B_ptr, device_C_ptr, N );


  // // Copy results from host to device
  // thrust::copy( device_C.begin(), device_C.end(), host_C.begin() );


  // // Free device memory
  // cudaFree( &device_A_ptr );
  // cudaFree( &device_B_ptr );
  // cudaFree( &device_C_ptr );


  // // Print results 
  // print( host_A );
  // std::cout << std::endl;

  // print( host_B );
  // std::cout << std::endl;

  // //print<double>( host_C );
  // std::cout << std::endl;

  return 0;

}

// END OF FILE
