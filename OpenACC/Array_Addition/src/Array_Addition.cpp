/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Array_Addition.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 05/03/2013
PROJECT    : CUDA practice
DESCRIPTION: Add two arrays
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>


void print( const int* v, int size ){

  for( int i = 0; i < size; ++i )
    std::cout << v[i] << " ";

}


__global__ void cuda_add( const int* v1,
		   const int* v2,
		   int* v3,
		   int size ){

  int global_thread_id = blockIdx.x * blockDim.x + threadIdx.x;

  if( global_thread_id < size )
    v3[global_thread_id] = v1[global_thread_id] + v2[global_thread_id];
}

int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  unsigned int N = 10;

  int host_A[10] = {0,1,2,3,4,5,6,7,8,9};
  int host_B[10] = {100,101,102,103,104,105,106,107,108,109};
  int host_C[10];

  // Print array contents before GPU computation
  print( host_A, 10 );
  std::cout << std::endl;

  print( host_B, 10 );
  std::cout << std::endl;

  print( host_C, 10 );
  std::cout << std::endl;

  ///////////////////////////////////////////////////////////////////////////////
  // Device data                                                               //
  ///////////////////////////////////////////////////////////////////////////////

  // Create pointers to copy to device memory
  int* device_A; 
  int* device_B;
  int* device_C;


  // Allocate memory on device
  unsigned int size = N * sizeof( int );
  cudaMalloc( (void **)&device_A, size );
  cudaMalloc( (void **)&device_B, size );
  cudaMalloc( (void **)&device_C, size );

  
  // Copy inputs from host to device
  cudaMemcpy( device_A, host_A, size, cudaMemcpyHostToDevice);
  cudaMemcpy( device_B, host_B, size, cudaMemcpyHostToDevice);


  // Call CUDA kernel
  cuda_add<<<1, 32>>>( device_A, device_B, device_C, size );


  // Copy results from host to device
  cudaMemcpy( host_C, device_C, size, cudaMemcpyDeviceToHost);


  // Free device memory
  cudaFree( &device_A );
  cudaFree( &device_B );
  cudaFree( &device_C );


  // Print results 
  print( host_A, 10 ); // Hasn't changed
  std::cout << std::endl;

  print( host_B, 10 ); // Hasn't changed
  std::cout << std::endl;

  print( host_C, 10 );
  std::cout << std::endl;

  return 0;

}

// END OF FILE
