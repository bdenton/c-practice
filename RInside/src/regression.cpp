/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : regression.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 06/04/2013
PROJECT    : RInside practice
DESCRIPTION: Pass data from C++ to R. Retrieve results from R-side computations.
///////////////////////////////////////////////////////////////////////////////*/

#include <RInside.h>               // for the embedded R via RInside

int main(int argc, char *argv[]) {

    RInside R(argc, argv);         // create an embedded R instance
    SEXP x_cpp, y_cpp, fit_cpp;    // Declare analogs to R-side variables

    // Call runif to generate random data.
    // The data returned from R are stored in C++ variables y_cpp and x_cpp.
    R.parseEval( "runif(50)", y_cpp );
    R.parseEval( "runif(50)", x_cpp );

    // The values of the C++ variables must be mirrored in the namespace of the
    // embedded R instance R functions can access them.
    R.assign( y_cpp, "y_R" );
    R.assign( x_cpp, "x_R" );

    // Once the variables are populated in the R namespace any R function can be
    // called. Again, the results returned from R are stored in a C++ variable, 
    // fit_cpp. Assign this value in the R namespace as well.
    R.parseEval( "glm( y_R ~ x_R )", fit_cpp );
    R.assign( fit_cpp, "fit_R" );

    // Print the results. parseEvalQ (Quiet?) doesn't assign a value returned 
    // from R to a C++ variable. It just prints results directly to the console. 

    R.parseEvalQ( "print(summary(fit_R))" );  

    return 0;
}

// END OF FILE
