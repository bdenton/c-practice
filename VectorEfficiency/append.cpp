#include <iostream>
#include <vector>

template< class T >
void print( std::vector< T > vec ){

  for( unsigned int i = 0; i < vec.size(); ++i )
    std::cout << vec[i] << " ";

  std::cout << std::endl;

} 


template< class T >
void append( std::vector< T >& a, const std::vector< T >& b ){

  a.insert( a.end(), b.begin(), b.end() );

} 

int main(){

  std::vector<int> A = std::vector<int>(3,10);
  std::vector<int> B = std::vector<int>(3,20);

  print<int>( A );
  print<int>( B );

  append<int>( A, B );

  print<int>( A );

  return 0;
}
