/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Maps.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/07/2013
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <map>
#include <string>

template < class K, class V >
void erase_by_key( std::map< K, V >& map, V value ){
  
  while( map.count( value ) > 0 ){
    typename std::map< K, V >::iterator it = map.find( value );
    map.erase( it );
  }
  
}


int main(){

  std::map< int, std::string > map1;

  map1[1] = "one";
  map1[2] = "two";
  map1[2] = "two again";

  std::map< int, std::string >::iterator it = map1.begin();

  while( it != map1.end() ){

    std::cout << it->first << "\t" << it->second << std::endl;

    ++it;

  }
  

  std::cout << std::endl;

  return 0;

}

//END OF FILE
