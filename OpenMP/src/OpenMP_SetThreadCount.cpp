/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : OpenMP_SetThreadCount.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/22/2013
PROJECT    : 
DESCRIPTION: Practice parallel C++ programming with OpenMP library.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <omp.h>
#include <unistd.h> 

int main( int argc, char *argv[] ){

  int np = 0;
  unsigned long int x = 0;

  omp_set_num_threads(5);

#pragma omp parallel default(shared) //private(np)
  {
    #if defined (_OPENMP)
      np = omp_get_num_threads();
    #endif

    while( x < 5E9 )
       x += 1;
  }

  std::cout << "Number of threads: " << np << std::endl;
  // This will report 0 (or whatever it is initialized to) if np is declared
  // private.

  return 0;

}



//END OF FILE
