/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : OpenMP_HelloWorld.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/22/2013
PROJECT    : 
DESCRIPTION: Practice parallel C++ programming with OpenMP library.
///////////////////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <omp.h>

int main( int argc, char *argv[] ){

  int iam = 0, np = 1;

  #pragma omp parallel default(shared) private(iam, np)
  {
    #if defined (_OPENMP)
      np = omp_get_num_threads();
      iam = omp_get_thread_num();
    #endif
    printf("Hello from thread %d out of %d\n", iam, np);
  }

  return 0;

}



//END OF FILE
