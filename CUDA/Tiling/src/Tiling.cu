/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Tiling.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/02/2014
PROJECT    : CUDA practice
DESCRIPTION: 
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <iomanip>
#include <cuda.h>

#include "../../utils/cuPrintf.cu"


// Define thread block dimensions
#define BLOCK_DIM_X 2
#define BLOCK_DIM_Y 2
#define BLOCK_DIM_Z 1

// Define tile width
#define TILE_WIDTH BLOCK_DIM_X // the tiling algorithm is simpler when the 
                               // TILE_WIDTH dimensions equal the block dimensions

__global__ void tiles( const int* a,
                       unsigned int a_rows,
                       unsigned int a_cols ){
  
  
  __shared__ int shared_A[TILE_WIDTH][TILE_WIDTH];
  
  // Load automatic variables blockIdx and threadIdx into registers for fast access
  unsigned int bx = blockIdx.x;
  unsigned int by = blockIdx.y;
  unsigned int tx = threadIdx.x;
  unsigned int ty = threadIdx.y;
  
  //unsigned int Row = by * BLOCK_DIM_Y + ty;
  // BLOCK_DIM_X + tx;
  
  int A_val = 0;
  unsigned int A_index = 0;
  
  // Demonstrate mapping threads to tiles. Print examples as in Thread_Organization
  for( unsigned int m = 0; m < (a_cols-1)/TILE_WIDTH + 1; m++ ){

     unsigned int Row = by * TILE_WIDTH + ty;
     unsigned int Col = m * TILE_WIDTH + tx;

    /////////////////////////////////////////////////////////////////////////////
    // Load data from input array 'a' into the tile arrays                     //
    /////////////////////////////////////////////////////////////////////////////
    
    A_val = 0;
    A_index = Row*a_cols + m*TILE_WIDTH + tx;

    //cuPrintf( "Row:%d Col:%d bx:%d TILE_WIDTH:%d tx:%d A_index:%d\n", Row, Col, bx, TILE_WIDTH, tx, A_index );

    // Check that global thread indices do not run over array bounds
    // Only populate tile elements that cover a portion of the array
    if( Row < a_rows && Col < a_cols && A_index < a_rows * a_cols )
      A_val = a[ A_index ];
    
    shared_A[ty][tx] = A_val;// The use of threadIdx values as indices in the 
                             // two-dimensional shared array is possible
                             // because the blocks and tiles have the same
                             // dimensions. If this were not the case, a more
                             // complex translation from threadIdx to tile array
                             // index would be necessary.

    // Print iterations of one tile across the input matrix
    if( m == 2 ){
      if( bx == 0 && Row == 0 && Col == 0 )// Only print tile header once
        cuPrintf( "\n*** TILE: %d ***", m );// tile header
      if( ty == 0 && tx == 0 )
        cuPrintf( "\n" );
      cuPrintf( "m:%d ty:%d tx:%d Row:%d Col:%d a[%d]:%d shared_A[%d][%d]:%d\n", m, ty, tx, Row, Col, A_index, A_val, ty, tx,shared_A[ty][tx] );
    }
  }
}
  
int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  unsigned int A_rows = 4, A_cols = 5;

  // Allocate memory on host
  int* host_A = (int*)malloc( A_rows * A_cols * sizeof( int ) );

  // Initialize input arrays
  for( unsigned int i = 0; i < A_rows * A_cols; i++ )
    host_A[i] = i;

  ///////////////////////////////////////////////////////////////////////////////
  // Device data                                                               //
  ///////////////////////////////////////////////////////////////////////////////
  
  //Allocate memory on device
  int* device_A;

  cudaMalloc( (void **)&device_A, A_rows * A_cols * sizeof(int) );
 
  // Copy inputs from host to device
  cudaMemcpy( device_A, host_A, A_rows * A_cols * sizeof(int), cudaMemcpyHostToDevice);// Host->Device

  // The dimensions of the thread blocks were specified earlier via the
  // #define statements.
  dim3 dimBlock( BLOCK_DIM_X, BLOCK_DIM_Y, BLOCK_DIM_Z );

  // The dimensions of the grid will be dictated by the size of the output matrix
  // and the dimensions of the thread blocks. The number of blocks required is 
  // the minumum number required to cover the output matrix with thread blocks in
  // the x and y dimensions (and z where applicable).
  unsigned int NUM_BLOCKS_X = (A_cols - 1)/BLOCK_DIM_X + 1;
  unsigned int NUM_BLOCKS_Y = (A_rows - 1)/BLOCK_DIM_Y + 1;

  dim3 dimGrid( NUM_BLOCKS_X, NUM_BLOCKS_Y, 1 );

  std::cout << "NUM_BLOCKS_X: " << NUM_BLOCKS_X << std::endl 
            << "NUM_BLOCKS_Y: " << NUM_BLOCKS_Y << std::endl << std::endl;

  // Call CUDA kernel
  cudaPrintfInit(); // enable printing to stdout from device
  tiles<<< dimGrid, dimBlock >>>( device_A, A_rows, A_cols );
  cudaPrintfDisplay(stdout, true); // flush device buffer
  cudaPrintfEnd(); // stop printing from device

  // Free device memory
  cudaFree( &device_A );

  std::cout << std::endl << std::endl;

  return 0;

}

// END OF FILE
