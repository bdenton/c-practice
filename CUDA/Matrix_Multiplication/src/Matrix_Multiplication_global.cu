/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Matrix_Multiplication_global.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/03/2013
PROJECT    : CUDA practice
DESCRIPTION: Multiply two vectors.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <iomanip>
#include <cuda.h>

//#include "../../utils/cuPrintf.cu"


// Define thread block dimensions
#define BLOCK_DIM_X 5
#define BLOCK_DIM_Y 5
#define BLOCK_DIM_Z 1

__global__ void multiply( const int* a,
                          const int* b,
                          int* c,
                          unsigned int a_rows,
                          unsigned int a_cols,
                          unsigned int b_cols ){
  
  // The result matrix will have dimensions (a_rows) X (b_cols).
  // a_cols and b_rows must be equal.
  // A[a_rows,a_cols] X B[b_rows,b_cols] = C[a_rows,b_cols], where a_cols == b_rows

  unsigned int tx = blockIdx.x * blockDim.x + threadIdx.x; // global thread x-index
  unsigned int ty = blockIdx.y * blockDim.y + threadIdx.y; // global thread y-index

  if( tx < b_cols && ty < a_rows ){

    int product = 0;
    
    for( unsigned int k = 0; k < a_cols; k++ ){
      product += a[ ty * a_cols + k ] * b[ k*b_cols + tx ];
    }
    
    c[ ty*b_cols + tx ] = product;
    
    //cuPrintf( "[ty*b_cols + tx]: %d\n", ty*b_cols + tx );
  } 
}


int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  unsigned int A_rows = 4, A_cols = 5;
  unsigned int B_rows = A_cols, B_cols = 6;
  unsigned int C_rows = A_rows, C_cols = B_cols;

  // Allocate memory on host
  int* host_A = (int*)malloc( A_rows * A_cols * sizeof( int ) );
  int* host_B = (int*)malloc( B_rows * B_cols * sizeof( int ) );
  int* host_C = (int*)malloc( C_rows * C_cols * sizeof( int ) );

  // Initialize input arrays
  for( unsigned int i = 0; i < A_rows * A_cols; i++ )
    host_A[i] = 1;

  for( unsigned int i = 0; i < B_rows * B_cols; i++ )
    host_B[i] = 2;

  ///////////////////////////////////////////////////////////////////////////////
  // Device data                                                               //
  ///////////////////////////////////////////////////////////////////////////////
  
  //Allocate memory on device

  int* device_A;
  int* device_B;
  int* device_C;
  cudaMalloc( (void **)&device_A, A_rows * A_cols * sizeof(int) );
  cudaMalloc( (void **)&device_B, B_rows * B_cols * sizeof(int) );
  cudaMalloc( (void **)&device_C, C_rows * C_cols * sizeof(int) );

  // Copy inputs from host to device
  cudaMemcpy( device_A, host_A, A_rows * A_cols * sizeof(int), cudaMemcpyHostToDevice);// Host->Device
  cudaMemcpy( device_B, host_B, B_rows * B_cols * sizeof(int), cudaMemcpyHostToDevice);// Host->Device


  // The dimensions of the thread blocks were specified earlier via the
  // #define statements.
  dim3 dimBlock( BLOCK_DIM_X, BLOCK_DIM_Y, BLOCK_DIM_Z );

  // The dimensions of the grid will be dictated by the size of the output matrix
  // and the dimensions of the thread blocks. The number of blocks required is 
  // the minumum number required to cover the output matrix with thread blocks in
  // the x and y dimensions (and z where applicable).
  unsigned int NUM_BLOCKS_X = (C_cols - 1)/BLOCK_DIM_X + 1;
  unsigned int NUM_BLOCKS_Y = (C_rows - 1)/BLOCK_DIM_Y + 1;

  dim3 dimGrid( NUM_BLOCKS_X, NUM_BLOCKS_Y, 1 );

  std::cout << "NUM_BLOCKS_X: " << NUM_BLOCKS_X << std::endl 
            << "NUM_BLOCKS_Y: " << NUM_BLOCKS_Y << std::endl;

  // Call CUDA kernel
  //cudaPrintfInit(); // enable printing to stdout from device
  multiply<<< dimGrid, dimBlock >>>( device_A, device_B, device_C, A_rows, A_cols, B_cols );
  //cudaPrintfDisplay(stdout, true); // flush device buffer
  //cudaPrintfEnd(); // stop printing from device


  // Copy results from device back to host
  cudaMemcpy( host_C, device_C, C_rows * C_cols * sizeof(int), cudaMemcpyDeviceToHost);// Device->Host

  // Free device memory
  cudaFree( &device_A );
  cudaFree( &device_B );
  cudaFree( &device_C );

  std::cout << std::endl << std::endl;
  std::cout << "*** Result from device computation ***" << std::endl << std::endl;
  for( unsigned int i = 0; i < C_rows; i++ ){
    for( unsigned int j = 0; j < C_cols; j++ ){
      //std::cout << i*C_cols + j << " ";
      std::cout << std::setw(6) << std::right << host_C[ i*C_cols + j ];
    }
    std::cout << std::endl;
  }

  std::cout << std::endl;

  return 0;

}

// END OF FILE
