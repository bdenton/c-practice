/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Matrix_Multiplication.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/03/2013
PROJECT    : CUDA practice
DESCRIPTION: Multiply two vectors.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>

#include <stdlib.h>
#include <time.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
//#include "../../utils/cuPrintf.cu"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#define BLOCK_WIDTH 16
#define DATA_TYPE int
#define PRINT_FIELD_WIDTH 6

// TODO: 1. Add preprocessor directives to input size here
//       2. Add optional dimension limits for printing functions

template <class T>
void print( const std::vector< std::vector< T > >& mat ){

  for( unsigned int i = 0; i < mat.size(); ++i ){
    for( unsigned int j = 0; j < mat[0].size(); ++j ){
      std::cout << std::right << std::setw( PRINT_FIELD_WIDTH ) << mat[i][j];
    }
    std::cout << std::endl;
  }
}


template <class T>
void print( const std::vector< T >& vec ){

  for( unsigned int i = 0; i < vec.size(); ++i ){
    std::cout << std::right << std::setw( PRINT_FIELD_WIDTH ) << vec[i];
  }
  std::cout << std::endl;
}

//TODO: Overload transpose function for a flattened matrix. 
//      Make transpose function for flattened matrices a CUDA kernel?
//      Can a CUDA kernel call another CUDA kernel? Does this pose any problems?

template < class T >
std::vector< std::vector< T > > transpose( const std::vector< std::vector< T > >& mat ){

  unsigned int rows = mat.size();
  unsigned int cols = mat[0].size();

  std::vector< std::vector< T > > transposed( cols, std::vector< T >( rows ) );

  for( unsigned int i = 0; i < cols; i++ ){
    for( unsigned int j = 0; j < rows; j++ ){
      transposed[i][j] = mat[j][i];
    }
  }

  return transposed;
}


template < class T >
std::vector< T > flatten( const std::vector< std::vector< T > >& mat ){

  unsigned int rows = mat.size();
  unsigned int cols = mat[0].size();
  std::vector< T > flat( rows * cols );

  for( unsigned int i = 0; i < rows; i++ ){
    for( unsigned int j = 0; j < cols; j++ ){
      flat[ i*cols + j ] = mat[i][j];
    }
  }

  return flat;
}



template < class T >
std::vector< std::vector< T > > unflatten( const std::vector< T >& flat, 
                                           unsigned int rows, 
                                           unsigned int cols ){

  std::vector< std::vector< T > > mat( rows, std::vector< T >( cols ) );

  for( unsigned int i = 0; i < rows; i++ ){
    for( unsigned int j = 0; j < cols; j++ ){
      mat[i][j] = flat[ i*cols + j ];
    }
  }

  return mat;
}

template <class T>
thrust::host_vector< T > STL_2_thrust_host_vector( const std::vector< T >& vec ){

  thrust::host_vector< T > host_vec( vec.size() );

  for( unsigned int i = 0; i < vec.size(); ++i )
    host_vec[i] = vec[i];

  return host_vec;

}

template <class T>
std::vector< T > thrust_host_vector_2_STL( const thrust::host_vector< T >& host_vec ){

  std::vector< T > vec( host_vec.size() );

  for( unsigned int i = 0; i < host_vec.size(); ++i )
    vec[i] = host_vec[i];

  return vec;

}


template < class T >
std::vector< std::vector< T > > host_multiply( const std::vector< std::vector< T > >& a,
                                               const std::vector< std::vector< T > >& b ){

  unsigned int a_rows = a.size();
  unsigned int a_cols = a[0].size();

  unsigned int b_rows = b.size();
  unsigned int b_cols = b[0].size();

  assert( a_cols == b_rows );


  std::vector< std::vector< T > > c( a_rows, std::vector< T >( b_cols ) );

  for( unsigned int i = 0; i < a_rows; i++ ){
    for( unsigned int j = 0; j < b_cols; j++ ){
      for( unsigned int k = 0; k < a_cols; k++ ){
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }

  return c;
}



template < class T >
std::vector< T > host_flat_multiply( const std::vector< T >& a,
                                                    const std::vector< T >& b,
                                                    unsigned int a_rows,
                                                    unsigned int a_cols,
                                                    unsigned int b_cols ){

  std::vector< T > c( a_rows * b_cols );

  for( unsigned int i = 0; i < a_rows; i++ ){
    for( unsigned int j = 0; j < b_cols; j++ ){

      DATA_TYPE product = 0;

      for( unsigned int k = 0; k < a_cols; k++ ){
        product += a[ i*a_cols + k ] * b[ k*b_cols + j ];
      }
      
      c[ i*b_cols + j ] = product;
    }
  }

  return c;
}


// Questions to consider:
// Currently flattening 2D arrays to 1D arrays before passing to the kernel 
// because it seems I need to know the dimensions of the array at compile time.
// So I am flattening the 2D arrays and passing the array dimensions as 
// additional parameters -- and they therefore do not need to be known at compile
// time. This necessitates either altering the algorithm to execute on a 1D array
// rather than the normal 2D configuration, or reconstructing the 2D array on the
// device. Is it possible to avoid this somehow and pass the 2D array to the 
// kernel without flattening first?  Also, does transposing the b matrix increase
// computation speed by coalescing memory accesses?

// Normal computation:   (ab)[i,j] = sum( a[i,k] * b[k,j], j = 1..a_col )

//                       This requires a scattered memory access pattern for the
//                       b matrix because you must access the jth element of 
//                       the kth row of a 2D array, where each row of the array
//                       is actually represented by its own 1D array (recall that
//                       a 2D array is actually an array of arrays). The jth 
//                       element of row k in matrix b is stored in memory that is
//                       distant from the jth element in row k+1. So memory
//                       accesses for this computation is scattered and many 
//                       threads in a warp will be wasted.


// Transposed b:         (ab)[i,j] = sum( a[i,k] * bT[j,k], j = 1..a_col )

//                       Note this formula uses bT rather than b, where bT is the
//                       transpose of b. This means the array indices in the
//                       formula can be switched and now summing over the index j
//                       gives coalesced memory accesses for both the a matrix
//                       and bT. The idea for this method came from reading 
//                       Ulrich Drepper's essay "What Every Programmer Should
//                       Know About Memory" Section 6.2.1 Optimizing
//                       Level 1 Data Cache Access. As the title implies, 
//                       Drepper's task in describing this method is to optimize
//                       CPU accesses to the L1 cache, but the similarity to GPU
//                       memory access is clear.




template < class T >
__global__ void device_multiply( const T* a,
                                 const T* b,
                                 T* c,
                                   unsigned int a_rows,
                                   unsigned int a_cols,
                                   unsigned int b_cols ){

// The result matrix will have dimensions (a_rows) X (b_cols).
// a_cols and b_rows must be equal.
// A[a_rows,a_cols] X B[b_rows,b_cols] = C[a_rows,b_cols], where a_cols == b_rows

  unsigned int i = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int j = blockIdx.x * blockDim.x + threadIdx.x;
  
  //cuPrintf( "i: %d\tj: %d\n", i, j );
  
  if( i < a_rows && j < b_cols ){
    
    DATA_TYPE product = 0;
    
    for( unsigned int k = 0; k < a_cols; k++ ){
      product += a[ i*a_cols + k ] * b[ k*b_cols + j ];
    }
    
    c[ i*b_cols + j ] = product;
  }
  
}


int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  // Define matrix dimensions
  unsigned int A_rows = 4, A_cols = 5;
  unsigned int B_rows = 5, B_cols = 6;
  
  // Assert conforming matrix dimensions
  assert( A_cols == B_rows );
  
  // Allocate matrices
  std::vector< std::vector< DATA_TYPE > > A(A_rows,std::vector< DATA_TYPE >(A_cols));
  std::vector< std::vector< DATA_TYPE > > B(B_rows,std::vector< DATA_TYPE >(B_cols));
  std::vector< std::vector< DATA_TYPE > > C(A_rows,std::vector< DATA_TYPE >(B_cols));

  // Populate matrices with values
  //DATA_TYPE val = 1;

  srand( time(NULL) );


  for( unsigned int i = 0; i < A_rows; ++i ){
    for( unsigned int j = 0; j < A_cols; ++j ){
      A[i][j] = (DATA_TYPE)( rand() % 100 );
      //val++;
    }
  }

  //val = 100;


  for( unsigned int i = 0; i < B_rows; ++i ){
    for( unsigned int j = 0; j < B_cols; ++j ){
      B[i][j] = (DATA_TYPE)( rand() % 100 );
      //val++;
    }
  }
  

  // Print 2D matrices
  print( A );
  std::cout << std::endl << std::endl;

  // print( transpose( A ) );
  // std::cout << std::endl << std::endl;
  


  print( B );
  std::cout << std::endl << std::endl;


  // print( transpose( B ) );
  // std::cout << std::endl << std::endl;
  

  // Flatten the matrices by appending the rows of the matrices
  std::vector< DATA_TYPE > flat_A = flatten( A );
  std::vector< DATA_TYPE > flat_B = flatten( B );
  std::vector< DATA_TYPE > flat_C = flatten( C );

  // Print flattened matrices
  // print( flat_A );
  // std::cout << std::endl << std::endl;

  // print( flat_B );
  // std::cout << std::endl << std::endl;

  // Wrap STL vectors in thrust::vectors
  thrust::host_vector< DATA_TYPE > host_A = STL_2_thrust_host_vector( flat_A );
  thrust::host_vector< DATA_TYPE > host_B = STL_2_thrust_host_vector( flat_B );
  thrust::host_vector< DATA_TYPE > host_C = STL_2_thrust_host_vector( flat_C );

  ///////////////////////////////////////////////////////////////////////////////
  // Device data                                                               //
  ///////////////////////////////////////////////////////////////////////////////

  // Allocate device memory and copy data from host
  thrust::device_vector< DATA_TYPE > device_A = host_A;
  thrust::device_vector< DATA_TYPE > device_B = host_B;
  thrust::device_vector< DATA_TYPE > device_C = host_C;

  
  // Thrust is a host-side abstraction only. You can create Thrust vectors and
  // other data types in memory, but you must pass C-style pointers to the 
  // CUDA kernel function. Thrust provides the function raw_pointer_cast() for the
  // purpose of creating pointers to Thrust data objects.
  
  // Create C-style pointers to Thrust device vectors
  DATA_TYPE* device_A_ptr = thrust::raw_pointer_cast( &device_A[0] );
  DATA_TYPE* device_B_ptr = thrust::raw_pointer_cast( &device_B[0] );
  DATA_TYPE* device_C_ptr = thrust::raw_pointer_cast( &device_C[0] );
  
  unsigned int NumBlocks = A_cols/BLOCK_WIDTH;
  

  // If size of kernel block is not a multiple of the width of the result matrix 
  // then increment the number of kernel blocks to be used in order to handle 
  // the overhang
  if( A_cols % BLOCK_WIDTH != 0 )
    NumBlocks++;
  
  dim3 dimGrid( NumBlocks, NumBlocks );
  dim3 dimBlock( BLOCK_WIDTH, BLOCK_WIDTH );

  // Call CUDA kernel
  //cudaPrintfInit(); // enable printing to stdout from device
  device_multiply<<< dimGrid, dimBlock >>>( device_A_ptr, device_B_ptr, device_C_ptr, A_rows, A_cols, B_cols );
  //cudaPrintfDisplay(stdout, true); // flush device buffer
  //cudaPrintfEnd(); // stop printing from device


  // Copy results from host to device
  thrust::copy( device_C.begin(), device_C.end(), host_C.begin() );


  // Free device memory
  cudaFree( &device_A_ptr );
  cudaFree( &device_B_ptr );
  cudaFree( &device_C_ptr );


  // Translate results from thrust::host_vector back to STL vector
  flat_C = thrust_host_vector_2_STL( host_C );

  // Unflatten results
  C = unflatten< DATA_TYPE >( flat_C, A_rows, B_cols );


  // Print results
  std::cout << "*** Result from 2D host computation ***" << std::endl << std::endl;
  print( host_multiply( A, B ) );
  std::cout << std::endl << std::endl;

  std::cout << "*** Result from flattened host computation ***" << std::endl << std::endl;
  print( unflatten( host_flat_multiply( flat_A, flat_B, A_rows, A_cols, B_cols ), A_rows, B_cols ) );
  std::cout << std::endl << std::endl;

  std::cout << "*** Result from device computation ***" << std::endl << std::endl;
  print( C );
  std::cout << std::endl;

  return 0;

}

// END OF FILE
