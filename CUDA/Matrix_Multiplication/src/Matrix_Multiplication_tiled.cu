/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Matrix_Multiplication_tiled.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/03/2013
PROJECT    : CUDA practice
DESCRIPTION: Multiply two vectors.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <iomanip>
#include <cuda.h>

#include "../../utils/cuPrintf.cu"


// Define thread block dimensions
#define BLOCK_DIM_X 2
#define BLOCK_DIM_Y 2
#define BLOCK_DIM_Z 1

// Define tile width
#define TILE_WIDTH BLOCK_DIM_X

__global__ void multiply( const int* A, 
                          const int* B, 
                          int* C, 
                          unsigned int A_rows, 
                          unsigned int A_cols,  
                          unsigned int B_cols ){

  unsigned int B_rows = A_cols;
  unsigned int C_rows = A_rows;
  unsigned int C_cols = B_cols;

  // Load automatic variables into registers for fast access
  unsigned int tx = threadIdx.x;
  unsigned int ty = threadIdx.y;
  unsigned int bx = blockIdx.x;
  unsigned int by = blockIdx.y;
  unsigned int Row = by*BLOCK_DIM_Y + ty;
  unsigned int Col = bx*BLOCK_DIM_X + tx;

  int product = 0;
  
  __shared__ int shared_A[TILE_WIDTH][TILE_WIDTH];
  __shared__ int shared_B[TILE_WIDTH][TILE_WIDTH];
  
  // Use this to inspect matrix values one tile at a time
  //for( unsigned int k = 2; k < 3; k++ ){
  for( unsigned int k = 0; k < (A_cols - 1)/TILE_WIDTH + 1; k++ ){
    
    // Scan tile across columns of A
    if( k*TILE_WIDTH + tx < A_cols && Row < A_rows){
      shared_A[ty][tx] = A[Row*A_cols + k*TILE_WIDTH + tx];
    }
    else{
      shared_A[ty][tx] = 0;
    }
    
    //Scan tile down rows of B
    if( k*TILE_WIDTH + ty < B_rows && Col < B_cols){
      shared_B[ty][tx] = B[(k*TILE_WIDTH + ty)*B_cols + Col];
    }
    else{
      shared_B[ty][tx] = 0;
    }
    
    __syncthreads();
    
    for( unsigned int k = 0; k < TILE_WIDTH; k++ ){
      product += shared_A[ty][k] * shared_B[k][tx];
    }
    
    __syncthreads();
    
  }
  
  if (Row < C_rows && Col < C_cols) 
    C[(by*BLOCK_DIM_Y + ty)*C_cols + (bx*BLOCK_DIM_X) + tx ] = product; 
}

// __global__ void multiply( const int* a,
//                           const int* b,
//                           int* c,
//                           unsigned int a_rows,
//                           unsigned int a_cols,
//                           unsigned int b_cols ){
  
//   // The result matrix will have dimensions (a_rows) X (b_cols).
//   // a_cols and b_rows must be equal.
//   // A[a_rows,a_cols] X B[b_rows,b_cols] = C[a_rows,b_cols], where a_cols == b_rows
  
//   __shared__ int shared_A[TILE_WIDTH][TILE_WIDTH];
//   __shared__ int shared_B[TILE_WIDTH][TILE_WIDTH];
  
//   unsigned int b_rows = a_cols;
  
//   // Load automatic variables blockIdx and threadIdx into registers for fast access
//   unsigned int bx = blockIdx.x;
//   unsigned int by = blockIdx.y;
//   unsigned int tx = threadIdx.x;
//   unsigned int ty = threadIdx.y;
  
//   // unsigned int Row = by * BLOCK_DIM_Y + ty;
//   // unsigned int Col = bx * BLOCK_DIM_X + tx;
  
//   int blockID = blockIdx.x + blockIdx.y * gridDim.x; 
//   int threadID = blockID * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;

//   // unsigned int threadID = threadIdx.x + (threadIdx.y * blockDim.x);// within a block
//   // unsigned int blockID = blockIdx.x + (blockIdx.y * gridDim.x);
  
//   // unsigned int tileIdx = Col/TILE_WIDTH;
//   // unsigned int tileIdy = Row/TILE_WIDTH;

//   int A_val = 0, B_val = 0;
//   unsigned int A_index = 0, B_index = 0;
  
//   int product = 0;

//   // Demonstrate mapping threads to tiles. Print examples as in Thread_Organization
//   for( unsigned int m = 0; m < (a_cols-1)/TILE_WIDTH + 1; m++ ){

//     unsigned int Row_A = by * BLOCK_DIM_Y + ty;
//     unsigned int Col_A = m * TILE_WIDTH + tx;

//     unsigned int Row_B = m * TILE_WIDTH + ty;
//     unsigned int Col_B = bx * BLOCK_DIM_X + tx;

//     /////////////////////////////////////////////////////////////////////////////
//     // Load data from input array 'a' into the tile arrays                     //
//     /////////////////////////////////////////////////////////////////////////////
    
//     A_val = 0;
//     A_index = Row_A*a_cols + m*TILE_WIDTH + tx;
//     if( Row_A < a_rows && Col_A < a_cols && A_index < a_rows * a_cols )
//       A_val = a[ A_index ];
    
//     shared_A[ty][tx] = A_val;
    
//     B_val = 0;
//     B_index = Row_B*b_cols + m*TILE_WIDTH + tx;

//     if( Row_B < b_rows && Col_B < b_cols && B_index < b_rows * b_cols )
//       B_val = b[ B_index ];
    
//     shared_B[tx][ty] = B_val;
 
//     if( m == 0 ){
//       if( bx == 0 && Row_A == 0 && Col_A == 0 )// Only print tile header once
//         cuPrintf( "\n*** TILE: %d ***", m );// tile header
//       if( ty == 0 && tx == 0 )
//         cuPrintf( "\n" );
//       cuPrintf( "blockIdx:%d threadIdx:%d Row:%d Col:%d a[%d]:%d shared_A[%d][%d]:%d\n", blockID, threadID, Row_A, Col_A, A_index, A_val, ty, tx, shared_A[ty][tx] );
//       //cuPrintf( "m:%d ty:%d tx:%d Row:%d Col:%d b[%d]:%d shared_B[%d][%d]:%d\n", m, ty, tx, Row_B, Col_B, B_index, B_val, ty, tx,shared_B[ty][tx] );
//     }
    
   
//     // Make sure loading data into tile arrays is complete before proceeeding
//     __syncthreads();
      
//     for( unsigned int k = 0; k < TILE_WIDTH; k++ ){
//       if( m == (a_cols-1)/TILE_WIDTH  )
//         cuPrintf( "[%d][%d]\tshared_A[%d][%d]:%d\tshared_B[%d][%d]:%d\n", blockID, threadID, ty,k,shared_A[ty][k],k,tx,shared_B[k][tx] );
//       product += shared_A[ty][k] * shared_B[k][tx];
//     }
      
//     __syncthreads();

//     c[ Row_A*b_cols + Col_A ] = product;
      
//   } 
// } 


int main(){

  ///////////////////////////////////////////////////////////////////////////////
  // Host data                                                                 //
  ///////////////////////////////////////////////////////////////////////////////

  unsigned int A_rows = 4, A_cols = 5;
  unsigned int B_rows = A_cols, B_cols = 6;
  unsigned int C_rows = A_rows, C_cols = B_cols;

  // Allocate memory on host
  int* host_A = (int*)malloc( A_rows * A_cols * sizeof( int ) );
  int* host_B = (int*)malloc( B_rows * B_cols * sizeof( int ) );
  int* host_C = (int*)malloc( C_rows * C_cols * sizeof( int ) );

  // Initialize input arrays
  for( unsigned int i = 0; i < A_rows * A_cols; i++ )
    host_A[i] = i;

  for( unsigned int j = 0; j < B_rows * B_cols; j++ )
    if( j % (B_cols + 1) == 0 )
      host_B[j] = 1;//100 + j;
    else
      host_B[j] = 1;

  ///////////////////////////////////////////////////////////////////////////////
  // Device data                                                               //
  ///////////////////////////////////////////////////////////////////////////////
  
  //Allocate memory on device

  int* device_A;
  int* device_B;
  int* device_C;
  cudaMalloc( (void **)&device_A, A_rows * A_cols * sizeof(int) );
  cudaMalloc( (void **)&device_B, B_rows * B_cols * sizeof(int) );
  cudaMalloc( (void **)&device_C, C_rows * C_cols * sizeof(int) );

  // Copy inputs from host to device
  cudaMemcpy( device_A, host_A, A_rows * A_cols * sizeof(int), cudaMemcpyHostToDevice);// Host->Device
  cudaMemcpy( device_B, host_B, B_rows * B_cols * sizeof(int), cudaMemcpyHostToDevice);// Host->Device


  // The dimensions of the thread blocks were specified earlier via the
  // #define statements.
  dim3 dimBlock( BLOCK_DIM_X, BLOCK_DIM_Y, BLOCK_DIM_Z );

  // The dimensions of the grid will be dictated by the size of the output matrix
  // and the dimensions of the thread blocks. The number of blocks required is 
  // the minumum number required to cover the output matrix with thread blocks in
  // the x and y dimensions (and z where applicable).
  unsigned int NUM_BLOCKS_X = (C_cols - 1)/BLOCK_DIM_X + 1;
  unsigned int NUM_BLOCKS_Y = (C_rows - 1)/BLOCK_DIM_Y + 1;

  dim3 dimGrid( NUM_BLOCKS_X, NUM_BLOCKS_Y, 1 );

  std::cout << "NUM_BLOCKS_X: " << NUM_BLOCKS_X << std::endl 
            << "NUM_BLOCKS_Y: " << NUM_BLOCKS_Y << std::endl;

  // Call CUDA kernel
  cudaPrintfInit(); // enable printing to stdout from device
  multiply<<< dimGrid, dimBlock >>>( device_A, device_B, device_C, A_rows, A_cols, B_cols );
  cudaPrintfDisplay(stdout, true); // flush device buffer
  cudaPrintfEnd(); // stop printing from device


  // Copy results from device back to host
  cudaMemcpy( host_C, device_C, C_rows * C_cols * sizeof(int), cudaMemcpyDeviceToHost);// Device->Host

  // Free device memory
  cudaFree( &device_A );
  cudaFree( &device_B );
  cudaFree( &device_C );

  std::cout << std::endl << std::endl;
  std::cout << "*** Matrix A ***" << std::endl << std::endl;
  for( unsigned int i = 0; i < A_rows; i++ ){
    for( unsigned int j = 0; j < A_cols; j++ ){
      std::cout << std::setw(6) << std::right << host_A[ i*A_cols + j ];
    }
    std::cout << std::endl;
  }
  

  std::cout << std::endl << std::endl;
  std::cout << "*** Matrix B ***" << std::endl << std::endl;
  for( unsigned int i = 0; i < B_rows; i++ ){
    for( unsigned int j = 0; j < B_cols; j++ ){
      std::cout << std::setw(6) << std::right << host_B[ i*B_cols + j ];
    }
    std::cout << std::endl;
  }
  
  
  std::cout << std::endl << std::endl;
  std::cout << "*** Result from device computation ***" << std::endl << std::endl;
  for( unsigned int i = 0; i < C_rows; i++ ){
    for( unsigned int j = 0; j < C_cols; j++ ){
      std::cout << std::setw(6) << std::right << host_C[ i*C_cols + j ];
    }
    std::cout << std::endl;
  }
  
  std::cout << std::endl;

  return 0;

}

// END OF FILE
