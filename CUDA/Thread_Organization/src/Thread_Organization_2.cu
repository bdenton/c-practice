/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Thread_Organization.cu
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 02/19/2014
PROJECT    : CUDA practice
DESCRIPTION: Demonstrate global, block, and thread ID.
See: http://www.martinpeniak.com/index.php?option=com_content&view=article&catid=17:updates&id=288:cuda-thread-indexing-explained
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>

#include <cuda.h>
#include <cuda_runtime_api.h>

// Define grid and block dimensions
#define GRID_DIM_X 3
#define GRID_DIM_Y 1
#define GRID_DIM_Z 1 // Must be 1 on older hardware.

#define BLOCK_DIM_X 3
#define BLOCK_DIM_Y 3
#define BLOCK_DIM_Z 1


__global__ void get_id( int* global,
                        int* block,
                        int* thread,
                        unsigned int size ){


  int blockX  = blockIdx.x;
  int blockY  = blockIdx.y * gridDim.x;
  int blockZ  = blockIdx.z * gridDim.x * gridDim.y;

  int blockId = blockX + blockY + blockZ;

  int threadX  = threadIdx.x;
  int threadY  = threadIdx.y * blockDim.x;

  int number_threads_in_previous_blocks = blockId * (blockDim.x * blockDim.y * blockDim.z);
  int thread_z_index_in_current_block = threadIdx.z * blockDim.x * blockDim.y;
  int threadZ = number_threads_in_previous_blocks + thread_z_index_in_current_block;

  int global_thread_id = threadX + threadY + threadZ;

  if( global_thread_id < size ){
    global[global_thread_id] = global_thread_id;
    block[global_thread_id] = blockId;
    thread[global_thread_id] = threadX + threadY + thread_z_index_in_current_block; //threadId within a block
  }
}


int main(){

  int N = GRID_DIM_X * GRID_DIM_Y * GRID_DIM_Z * BLOCK_DIM_X * BLOCK_DIM_Y * BLOCK_DIM_Z;

  int* global_host;
  int* global_device;

  int* block_host;
  int* block_device;

  int* thread_host;
  int* thread_device;

  unsigned int size = N * sizeof( int );

  // Allocate memory on host
  global_host = (int *)malloc( size );
  block_host = (int *)malloc( size );
  thread_host = (int *)malloc( size );
  //threadY_host = (int *)malloc( size );

  // Allocate memory on device;
  cudaMalloc( (void **)&global_device, size );
  cudaMalloc( (void **)&block_device, size );
  cudaMalloc( (void **)&thread_device, size );

  // Grid and Block dimensions

  // *** CUDA kernel signature ***
  // kernel<< dim3 GRID_DIM, dim3 BLOCK_DIM >>(...);
  // GRID_DIM and BLOCK_DIM are structs of type dim3. GRID_DIM specifies the 
  // number of blocks in each dimension of the grid. BLOCK_DIM specifies the 
  // number of threads in each dimension of each thread block. Grids and blocks
  // may be one-dimensional, two-dimensional, or (on newer devices)
  // three-dimensional.

  // example: dim3 GRID_DIM = dim3(10,10,1) is a two-dimensional grid with 10
  //          thread blocks in the x-dimension, 10 thread blocks in the 
  //          y-dimension, and 1 thread block in the z-dimension.

  // example: dim3 BLOCK_DIM = dim3(5,4,3) is a three-dimensional thread block 
  //          with 5 threads in the x-dimension, 4 threads in the y-dimension, 
  //          and 3 threads in the z-dimension.

  // Putting these grid and block dimensions together would launch 6,000 threads.
  // 10 * 10 * 1 * 5 * 4 * 3 = 6000


  // *** CUDA kernel signature 2 ***
  // kernel_call<< int NUMBER_OF_BLOCKS, int BLOCK_X_DIM, int BLOCK_Y_DIM >>(...);

  // Becuase the most common grid/thread configuration is a one-dimensional grid
  // containing some number of two-dimensional thread blocks, a second signature
  // is provided as a convenience. This signature allows the user to simply 
  // specify the number of two-dimensional thread blocks and then the x- and 
  // y-dimensions of those blocks. This was the signature used in 
  // Thread_Organization_1.cu:
  // kernel<< int NUMBER_OF_BLOCKS, int BLOCK_X_DIM, int BLOCK_Y_DIM >>(...);

  // Call CUDA kernel
  dim3 GRID_DIM  = dim3( GRID_DIM_X, GRID_DIM_Y, GRID_DIM_Z );// number of blocks in grid
  dim3 BLOCK_DIM = dim3( BLOCK_DIM_X, BLOCK_DIM_Y, BLOCK_DIM_Z );// number of threads in block

  get_id<<< GRID_DIM, BLOCK_DIM >>>( global_device, block_device, thread_device, size );


  // Copy results from host to device
  cudaMemcpy( global_host, global_device, size, cudaMemcpyDeviceToHost);
  cudaMemcpy( block_host, block_device, size, cudaMemcpyDeviceToHost);
  cudaMemcpy( thread_host, thread_device, size, cudaMemcpyDeviceToHost);

  // Print results

  int TOTAL_THREAD_COUNT = GRID_DIM_X * GRID_DIM_Y* GRID_DIM_Z *
                           BLOCK_DIM_X * BLOCK_DIM_Y * BLOCK_DIM_Z;
    
  std::cout << "GRID_DIM_X: " << GRID_DIM_X << std::endl
            << "GRID_DIM_Y: " << GRID_DIM_Y << std::endl
            << "GRID_DIM_Z: " << GRID_DIM_Z << std::endl
            << std::endl 
            << "BLOCK_DIM_X: " << BLOCK_DIM_X << std::endl
            << "BLOCK_DIM_Y: " << BLOCK_DIM_Y << std::endl
            << "BLOCK_DIM_Z: " << BLOCK_DIM_Z << std::endl 
            << "Total thread count: " << TOTAL_THREAD_COUNT << std::endl << std::endl;


  for( unsigned int bz = 0; bz < GRID_DIM_Z; bz++ ){
    for( unsigned int bx = 0; bx < GRID_DIM_X; bx++ ){
      for( unsigned int by = 0; by < GRID_DIM_Y; by++ ){


        std::cout << "BLOCK(" << bx << "," << by << "," << bz << "):THREAD(x,y,0)" << std::endl;

        for( unsigned int tz = 0; tz < BLOCK_DIM_Z; tz++ ){

          // if thread block is three-dimensional print next x-y slice
          if( tz > 0 ){
            std::cout << std::endl;
            std::cout << "BLOCK(" << bx << "," << by << "," << bz << "):THREAD(x,y," << tz << ")" << std::endl;
          }
          
          for( unsigned int tx = 0; tx < BLOCK_DIM_X; tx++ ){
            for( unsigned int ty = 0; ty < BLOCK_DIM_Y; ty++ ){

              int blockY  = by;
              int blockX  = bx * GRID_DIM_Y;
              int blockZ  = bz * GRID_DIM_X * GRID_DIM_Y;
              
              int blockId = blockX + blockY + blockZ;
             
              int threadY  = ty;
              int threadX  = tx * BLOCK_DIM_Y;
              
              int number_threads_in_previous_blocks = blockId * (BLOCK_DIM_X * BLOCK_DIM_Y * BLOCK_DIM_Z );
              int thread_z_index_in_current_block = tz * BLOCK_DIM_X * BLOCK_DIM_Y;
              int threadZ = number_threads_in_previous_blocks + thread_z_index_in_current_block;
              
              int global_thread_id = threadX + threadY + threadZ;

              //std::cout << global_thread_id << "\t";
              std::cout << global_host[global_thread_id] << "\t";
              //std::cout << block_host[global_thread_id] << "\t";
              //std::cout << thread_host[global_thread_id] << "\t";

            }// end block.y
            std::cout << std::endl;// end of thread block row
          }// end block.x
        }// end block.z
        std::cout << std::endl;
      }//end grid.y
    }// end grid.x
  }//end grid.z

  // free the pointers
  free( global_host );
  cudaFree( &global_device );

  free( block_host );
  cudaFree( &block_device );

  free( thread_host );
  cudaFree( &thread_device );

  return 0;

}

// END OF FILE
