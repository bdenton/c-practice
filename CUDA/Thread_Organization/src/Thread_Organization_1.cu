/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Thread_Organization.cu
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 02/19/2014
PROJECT    : CUDA practice
DESCRIPTION: Demonstrate global, block, and thread ID.
See: http://www.martinpeniak.com/index.php?option=com_content&view=article&catid=17:updates&id=288:cuda-thread-indexing-explained
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>

#include <cuda.h>
#include <cuda_runtime_api.h>


__global__ void get_id( int* global,
                           int* block,
                           int* thread,
                           unsigned int size ){

  int global_thread_id = blockIdx.x * blockDim.x + threadIdx.x;

  if( global_thread_id < size ){
    global[global_thread_id] = global_thread_id;
    block[global_thread_id] = blockIdx.x;
    thread[global_thread_id] = threadIdx.x;
  }
}


int main(){

  int N = 10;

  int* global_host;
  int* global_device;

  int* block_host;
  int* block_device;

  int* thread_host;
  int* thread_device;

  unsigned int size = N * sizeof( int );

  // Allocate memory on host
  global_host = (int *)malloc( size );
  block_host = (int *)malloc( size );
  thread_host = (int *)malloc( size );

  // Allocate memory on device;
  cudaMalloc( (void **)&global_device, size );
  cudaMalloc( (void **)&block_device, size );
  cudaMalloc( (void **)&thread_device, size );

  // *** CUDA kernel signature ***
  // kernel<< int NUMBER_OF_BLOCKS, int BLOCK_X_DIM, int BLOCK_Y_DIM >>(...);

  // Call CUDA kernel
  get_id<<< 5, 2, 1 >>>( global_device, block_device, thread_device, size );

  // Copy results from host to device
  cudaMemcpy( global_host, global_device, size, cudaMemcpyDeviceToHost);
  cudaMemcpy( block_host, block_device, size, cudaMemcpyDeviceToHost);
  cudaMemcpy( thread_host, thread_device, size, cudaMemcpyDeviceToHost);

  // Print results
  std::cout << "block_id" << std::endl;
  for( unsigned int i = 0; i < N; i++ )
    std::cout << block_host[i] << "\t";
  std::cout << std::endl << std::endl;

  std::cout << "thread_id" << std::endl;
  for( unsigned int i = 0; i < N; i++ )
    std::cout << thread_host[i] << "\t";
  std::cout << std::endl << std::endl;

  std::cout << "global_id" << std::endl;
  for( unsigned int i = 0; i < N; i++ )
    std::cout << global_host[i] << "\t";
  std::cout << std::endl << std::endl;

  // free the pointers
  free( global_host );
  cudaFree( &global_device );

  free( block_host );
  cudaFree( &block_device );

  free( thread_host );
  cudaFree( &thread_device );

  return 0;

}

// END OF FILE
